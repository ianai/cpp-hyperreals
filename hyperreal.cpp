//============================================================================
// Name        : hyperreal.cpp
// Author      : Ian Schroeder-Anderson
// Version     : 0.1
// Copyright   : Just give me a tip of the hat somewhere.
// Description : My C++ HyperReal implementation.
//============================================================================

#include "hyperreal.h"
#include <iterator>
#include <array>
#include <vector>

hyperreal::hyperreal(void) {
	digits=std::vector<double>(NUM_DIGITS,0);
	standardPartExists = true;
	standardPart = (double) 0.0;
	isInc = true;
	isDec = true;
}


hyperreal::hyperreal(double init_val){
	digits=std::vector<double>(NUM_DIGITS, init_val);
	standardPartExists = true;
	standardPart = (double) init_val;
	//misc
	isInc = true;
	isDec = true;
}

hyperreal::hyperreal(std::vector<double>& init_vec) {
	digits = init_vec;
	isInc=this->isIncreasing();
	isDec=this->isDecreasing();
	standardPartExists = this->func_standardPartExists();
	standardPart = this->func_standardPart();

}



bool hyperreal::isDecreasing(void) {
	//see comment in isIncreasing
	bool value = 1;

	for(std::vector<double>::iterator itDigit = digits.begin()+1; itDigit !=digits.end(); ++itDigit ) {
		if( itDigit >= itDigit-1) {
			value=0;
		}

	}
	return(value);
}


bool hyperreal::isIncreasing(void) {
	//starts out assuming condition is true
	//on first violation of the digits increasing
	//the value is changed to false
	bool value = 1;

	for(std::vector<double>::iterator itDigit = digits.begin()+1; itDigit !=digits.end(); ++itDigit ) {
		if( itDigit <= itDigit-1) {
			value=0;
		}

	}
	return(value);
}

bool hyperreal::func_standardPartExists(void) {
	//starts out assuming standard part does not exist
	//tests for conditions in which standard part exists

	bool value = 0;

	if (isInc | isDec) {
		//ideally, this would test for a limit of the digits
		//to be implemented in the future?
		value = 1;
	}
	return(value);

}

double hyperreal::func_standardPart(void) {
	double value;
	value=double(0);
	//write this tomorrow:
	//max function that calculates max of an array of doubles
	//min function "	"	"			"	"

	if (isInc | isDec) {
		//I hope to replace this with an actual algorithm sometime.
		//Currently assuming the last entry in the array will be
		//the best approximation to the standard part
		//if the hyperreal sequence is monotonic.
		value=digits.at(NUM_DIGITS-1);
	}

	return(value);
}

bool hyperreal::operator== (hyperreal& other) {
	//currently a pass-through to the underlying vector operator
	bool value=1;

	value = (digits == other.digits);

	return(value);
}

bool hyperreal::operator<(hyperreal& other) {
	//currently a pass-through to the underlying vector operators.
	//
	bool value=0;

	if ((this->standardPartExists && other.standardPartExists) && (this->standardPart < other.standardPart) ) {
		//test the standard parts for the appropriate condition
		value=1;
	} else {
		value=(digits < other.digits);
	}

	return(value);
}

std::ostream& hyperreal::operator <<(std::ostream& input) {
	//for_each(digits.begin(), digits.end(), input.put);

	return(input<<digits.at(NUM_DIGITS-1));

}


