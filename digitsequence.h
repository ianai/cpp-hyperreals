//============================================================================
// Name        : digitsequence.h
// Author      : Ian Schroeder-Anderson
// Version     : 0.1
// Copyright   : Just give me a tip of the hat somewhere.
// Description : Header for a specialized class for handling the digits.
//				 Meant to be like a vector, but allows for overriding the method
//				 for calculating digits past the initial set of sequences (for limit algorithms maybe?)
//============================================================================

#ifndef DIGITSEQUENCE_H_
#define DIGITSEQUENCE_H_
#include <vector>

template<class T>//T should be a numeric class like double, and doubles MUST be convertible to whatever T is.
class DigitSequence {
public:
	std::vector<T> digits;
	const int minDigits = 30; //every DigitSequence will calculate this many places to start
	const int maxDigits = 1000; //the max any DigitSequence will calculate

	DigitSequence(void);
	DigitSequence(double);

	T operator [] (int);
	DigitSequence<T> operator + (DigitSequence<T>&);
	DigitSequence<T> operator * (DigitSequence<T>&);

protected:

	double calcDigit(int);

};



#endif /* DIGITSEQUENCE_H_ */
