//============================================================================
// Name        : hyperreal.h
// Author      : Ian Schroeder-Anderson
// Version     : 0.1
// Copyright   : Just give me a tip of the hat somewhere.
// Description : Header for my C++ HyperReal implementation.
//============================================================================


#ifndef HYPERREAL_H_
#define HYPERREAL_H_
#include <vector>
#include <ostream>

class hyperreal {
public:
	static const int NUM_DIGITS = 30; //The number of digits in a hyperreal's representation. Eventually this may be broken out to min_ and max_
	std::vector<double> digits; //this will hold our hyperreal's digits, i.e. the ( h(0), h(1), ..., h(MAX_DIGITS-1) ) representation
	//IMPORTANT! every hyperreal's digits array will be a vector of doubles - so passing it integers will still result in a vector of doubles
	bool isInc, isDec;
	bool standardPartExists;
	double standardPart;

	hyperreal(void) ;
	hyperreal(double);
	hyperreal(std::vector<double>&);

	//comparisons
	bool operator == (hyperreal&); //implemented from theory
	bool operator < (hyperreal&); //implemented from theory
	//implemented as extensions of the previous two
	bool operator > (hyperreal&);
	bool operator <= (hyperreal&);
	bool operator >= (hyperreal&);

	//operations implemented digit-wise
	hyperreal operator + (hyperreal&);
	hyperreal operator - (hyperreal&);
	hyperreal operator * (hyperreal&);
	hyperreal operator / (hyperreal&);
	std::ostream& operator<< (std::ostream&);

	//misc
	//the following two functions to be called by the constructors
	bool func_standardPartExists(void); //tests for existence
	double func_standardPart(void); //for now, a simple map from the hyperreal to reals.
	bool isIncreasing(void); //are the digits monotonic, increasing?
	bool isDecreasing(void); //"	"	"	"	"	"	, decreasing?


};


#endif /* HYPERREAL_H_ */
