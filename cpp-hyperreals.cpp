//============================================================================
// Name        : cpp-hyperreals.cpp
// Author      : Ian Schroeder-Anderson
// Version     : 0.1
// Copyright   : Just give me a tip of the hat somewhere.
// Description : "Unit testing" my C++ HyperReal implementation.
//
//Current 1/5/2014 output is:
//here are the digits of the first hyperreal:10
//here are the digits of the second hyperreal:81
//here are the digits of the infinitesimal hyperreal: 0.5 , ..., 9.31323e-10
//here are the digits of the infinite hyperreal:2 , ..., 1.07374e+09
//============================================================================

#include <iostream>
#include "hyperreal.h"
#include <vector>
#include <math.h>

#define EXIT_SUCCESS 0

int main(void) {
	hyperreal a =  hyperreal(10);
	hyperreal b =  hyperreal(81);

	std::vector<double> digitsInfinitesimal=std::vector<double>( (hyperreal::NUM_DIGITS) );

	//create an example infinitesimal hyperreal
	int digit=0;
	generate(digitsInfinitesimal.begin(), digitsInfinitesimal.end(), [&]{ digit++; return( pow(double(0.5), digit) ) ; } );
	hyperreal infinitesimal = hyperreal(digitsInfinitesimal);

	//create an example infinite hyperreal
	digit = 0;
	std::vector<double> digitsInfinite = std::vector<double>( (hyperreal::NUM_DIGITS) );
	generate(digitsInfinite.begin(), digitsInfinite.end(), [&]{digit++; return(pow(2,digit));});
	hyperreal infinite = hyperreal(digitsInfinite);

	std::cout << "here are the digits of the first hyperreal:" << a.digits.at(0) << "\n";
	std::cout << "here are the digits of the second hyperreal:" << b.digits.at(0) << "\n";

	std::cout << "here are the digits of the infinitesimal hyperreal: " << infinitesimal.digits.at(0) << " , ..., " << infinitesimal.digits.at( (hyperreal::NUM_DIGITS)-1) << "\n";
	//std::cout << "here are the digits of the infinite hyperreal:" << std::string( infinite.digits.begin(), infinite.digits.end() ) << "\n"; <-figure out why this doesn't work next
	std::cout << "here are the digits of the infinite hyperreal:" << infinite.digits.at(0) << " , ..., " << infinite.digits.at( (hyperreal::NUM_DIGITS)-1) << "\n";

	return EXIT_SUCCESS;
}
